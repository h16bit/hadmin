/**
 * Settings of the project
 */

var settings = {
    version: 'dev',
    user: '',
    password: '',
    modules: {
        'backbone/backbone':['underscore/underscore'],
        'jquery/jquery-1.7.2':[],
        //'redactor/redactor':['jquery/jquery-1.7.2'],
        'hadmin/hadmin':['../config/models']
    }
};