var Hadmin = function(){
};

/*
 Init module
 */
Hadmin.prototype.init = function(){
    this.menuBuilder();

    var req = this.parseUrlQuery();

    if(req.action == 'add'){
        this.buildForm(req.table);
    };

    if(req.action == 'list'){
        this.buildList(req.table);
    };
};

/*
Method for building menu
 */
Hadmin.prototype.menuBuilder = function(){
    var menuSource = [];

    for(var i = 0; i<db.length;i++){
        menuSource.push({
            text: db[i].menuTitle,
            //url: this.menuLinkBuilder(db[i].table),
            items: [
                {
                    text: 'Add',
                    url: this.menuLinkBuilder(i,'add')
                },
                {
                    text: 'List',
                    url: this.menuLinkBuilder(i)
                }
            ]
        });
    }

    $("#menu").kendoMenu({
        orientation: "vertical",
        dataSource: menuSource
    });
};

/*
Build link for menu
 */
Hadmin.prototype.menuLinkBuilder = function(table,action){
    var ac = action,
        tb = table;

    if(ac == undefined){
        ac = 'list';
    }

    return '?table='+tb+'&action='+ac;
};

/*
Get parameters parser
 */
Hadmin.prototype.parseUrlQuery = function(table,action){
    var data = {};
    if(location.search) {
        var pair = (location.search.substr(1)).split('&');
        for(var i = 0; i < pair.length; i ++) {
            var param = pair[i].split('=');
            data[param[0]] = param[1];
        }
    }
    return data;
}

/*
 Form builder
 */
Hadmin.prototype.buildForm = function(table){
    var fields = db[table].fields,
        field;

    this.addChild('center','form',{id: 'form'});

    for(var i = 0;i < fields.length; i++){
        var field = this.getFieldType(fields[i].type);
        var id = fields[i].name+table+'Value';

        field.options['data-bind'] = fields[i].name;
        field.options['id'] = id;

        var label = this.addChild('form','label',{text: fields[i].caption});
        this.addChild(label,field.tag,field.options);

        if (field.bind != undefined){
            $('#'+id)[field.bind]();
        }
    }
}

/*
 Add child for element
 */

Hadmin.prototype.addChild = function(id,el,options){
    var parent,
        e;
    if(typeof id == 'object'){
        parent = id;
    }
    else{
        parent = document.getElementById(id);
    }
    e = document.createElement(el);

    for(var i in options){
        if(i == 'text'){
            e.appendChild(document.createTextNode(options[i]));
        }
        else{
            e[i] = options[i];
        }
    }
    parent.appendChild(e);

    return e;
}

/*
 Add child for element
 */
Hadmin.prototype.getFieldType = function(type){
    var out = {options:{}};

    switch(type){
        case 'text':
            out = {
                tag: 'textarea',
                options: {
                    cols: 100,
                    rows: 20
                },
                bind: 'kendoEditor'
            };
            break;
        case 'string':
            out =  {
                tag: 'input',
                options: {
                    type: 'text'
                }
            };
            break;
    };

    return out;
}

/*
 Module activation
 */
$(document).ready(function(){
    var hd = new Hadmin();
    hd.init();
});