/*
    Dom constructor - DCS
*/
(function(){
    var DCS = function(){};

    /*
     Menu constructor
     ==================================================================================
     */

    //Create menu for app
    DCS.prototype.buildMenu = function(models){
        //Get template for menu item
        this.t_menu_link = $('#t_menu_link').html();
        this.menu = $('#menu');

        //Append items for all models
        for(var i= 0,l=models.length;i<l;i++){
            if(models[i] != undefined){
                this.addMenuItem(models[i].viewSettings.menuTitle,ModelList[i]);
            }
        }
    };

    //Add link into menu
    DCS.prototype.addMenuItem = function(text,link){
        this.menu.append(_.template(this.t_menu_link,{link:link,text:text}));
    };

    /*
     List constructor
     ==================================================================================
     */

    //Add field like in model description
    DCS.prototype.buildList = function(model,fields){
        $('#content').html(_.template($('#t_list').html(),{model:model,fields: fields}));
    };

    /*
     Form constructor
     ==================================================================================
     */

    //Add field like in model description
    DCS.prototype.addField = function(model){

    };

    //Build form for every model
    DCS.prototype.buildForm = function(models){
        for(var i in models){
            this.addField(models[i]);
        }
    };


    /*
     Initialization
     ==================================================================================
     */

    //Add DCS prototype to Hadmin prototype tree
    Hadmin.prototype.DCS = DCS.prototype;
})();
