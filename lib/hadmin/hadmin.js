/*
General module
 */
function Hadmin(){};

//Build carcas of application
Hadmin.prototype.globalInit = function(models){
    var self = this;

    this.DCS.buildMenu(models);
    Backbone.history.start();
};

//Initialize action paths
Hadmin.prototype.routerInit = function(name){
    var self = this;
    //Base paths
    var routes = {};
    routes[name] = 'getList';
    routes[name+'/add'] = 'formAdd';
    routes[name+ '/edit'] = 'formEdit';

    //Init paths for backbone
    var rt = Backbone.Router.extend({
        routes: routes,
        getList: function() {
            var view = new self.Views[name].list;
        },
        formAdd: function() {
            var view = new self.Views[name].form;
        },
        formEdit: function() {
            var view = new self.Views[name].form;
        }
    });

    this.Routers[name] = new rt;
};

//Initialize model for element
Hadmin.prototype.modelInit = function(name,model){
    if(model != undefined){
        this.Models[name] = Backbone.Model.extend(model.fields);
        this.Collections[name] = Backbone.Collection.extend({model: this.Models[name]});
    }
};

//Initialize view for element
Hadmin.prototype.viewInit = function(name){
    var self = this;
    this.Views[name] = {};

    //Initialize list for model
    this.Views[name].list = Backbone.View.extend({
        id: 'left',

        initialize: function() {
            self.DCS.buildList(name,self.ModelsConfig[name].viewSettings.listFields);
        },

        events: {
            'click': 'al'
        },

        al: function(){alert('1')}
    });

    //Initialize form for model
    this.Views[name].form = Backbone.View.extend({
        id: 'left',

        initialize: function() {
            this.render();
        },

        events: {
            'click': 'al'
        },

        render: function(){
            self.DCS.buildForm(self.Models);

            return this;
        },

        al: function(){alert('1')}
    });
};

Hadmin.prototype.init = function(){
    //For access to this namespace craete self link
    var self = this;
    //Define base storages for Backbone
    this.Models = {};
    this.Collections = {};
    this.Views = {};
    this.Routers = {};
    this.ModelsConfig = {};

    //Include DOM constructor module
    requirejs({baseUrl: 'lib'},['hadmin/dom']);

    //require all models and define them in models definition list
    requirejs({baseUrl: 'config/models'},ModelList,function(){
        //go in cicle by the all models list
        for(var i = 0; i< arguments.length; i++){
            var name = ModelList[i];
            self.ModelsConfig[name] = arguments[i];
            //Define models and views
            self.modelInit(name,arguments[i]);
            self.viewInit(name);
            self.routerInit(name);
        }
        self.globalInit(arguments);
    });

};

//Module initialization
var hd = new Hadmin();
hd.init();