/*
 Require files
 */
(function(global){
    //Configuration of require.js
    requirejs.config({
        //By default load any module IDs from js/lib
        baseUrl: 'lib',
        //except, if the module ID starts with "app",
        //load it from the js/app directory. paths
        //config is relative to the baseUrl, and
        //never includes a ".js" extension since
        //the paths config could be for a directory.
        paths: {
            config: '../config'
        },
        shim: getModulesDeps()
    });

    //Get basic dependencies
    requirejs(getModules());

    //Get modules for loading list
    function getModules(){
        var tmpArray = [];
        min  = settings.version == 'prod' ? '.min':'';

        for (var i in settings.modules){
            tmpArray.push(i+min);
        }

        return tmpArray;
    }

    //Get modules for dependencies list
    function getModulesDeps(){
        var tmpObj = {};
        min  = settings.version == 'prod' ? '.min':'';

        for (var i in settings.modules){
            if (settings.modules[i].length > 0)
                tmpObj[i+min] = settings.modules[i];
        }

        return tmpObj;
    }
})(window);